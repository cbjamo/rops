# Reading sensors and making ports

<picture of arty>

The arty is here, and I'm re-learing how to do digital logic, it's been a while since my intro class in college. Writing verilog is pretty alien for a C programmer. Everyone says that it's not like programming, and like you probably are now, I would nod at the sage advice of my predicesors, but I didn't really understand how different it is. Just because Verilog looks a little like c does not mean it's actually anything like it.

<logic diagram>
The first thing I want to get the hardware doing is reading from the IMU and Barometer, and spitting that data out on the uart. 

<SPI module digram>
To do that we'll need a module that can talk to the SPI ports of our sensors. The main parts of our SPI module are a shift register large enough for the whole transaction, a counter to sequence our logic, and an output register. Most of the rest of the logic is to handle SPI's CPOL and CPHA. The inputs of this module 

The barameter is a pretty straightforward setup. We'll configure it to continiously measure the pressure and temperature and send us an interrupt when new data is ready. This will kick off a transaction to pull the data over to the FPGA. Once the transaction is done, we load that data into an intermediate register to store it until we're ready to send it out on the UART.

The IMU, on the other hand is a bit more complicated. There are two SPI ports, one for the Accelerometer/Gyroscope, and one for the Magentometer. But, they share the same MOSI pin. This means that we can't transact with both at the same time. In practice, this won't matter, since we'll be clocking the SPI ports around 5Mhz, so bus utilization will be quite low. It's pretty annoying to deal with though, especially since they part has some pads that aren't connected to anything.

I'm going to deal with it by putting some logic in front of the transact enable pins of our SPI modules. The logic fairly simple. 
MAG_T_E = ~Mag_Busy * ~A/G_Busy * Mag_Int
A/G_T_E = ~Mag_Busy * ~A/G_Busy * ~Mag_Int * A/G_Int
