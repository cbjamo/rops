# Tcl, or how I learned to stop worrying, but still hate Vivado.

![Vivado](vivado.jpg)

Xilinx Vivado is a powerful IDE for working with FPGAs, it's free (as in beer) and well documented.
For a cli guy like myself, it's also pretty much my worst nightmare. 

## Version Control
By default, Vivado uses what they call Project Mode. 
Project mode is intended to be an all-in-one solution to FPGA design.
It's main downside, in my mind, is that it's esentially completely incompatable with version control.
It spews a massive filetree into your workspace, making it nearly impossible to use any kind of VCS.

## tcl, the solution
Lucklily, Vivado does have a Tcl interface to allow us to work around the IDE.
This isn't a magic bullet. As far as I can tell, calling vivado in tcl mode simply hides the gui, and importantly, provide a scripting interface.

build.sh

	#!/bin/bash
	source /opt/Xilinx/Vivado/2018.1/settings64.sh
	rm -rf xbuild/*
	vivado -source init.tcl -mode tcl -nolog -nojournal

init.tcl

	# Make the vivado project
	create_project arty_test xbuild -part xc7a35ticsg324-1L
	add_files -norecurse -force {top/}
	add_files -fileset constrs_1 -norecurse -force top/constraints.xdc
	update_compile_order -fileset sources_1

	# Build
	launch_runs impl_1 -to_step write_bitstream -jobs 2
	wait_on_run impl_1

	# Program FPGA
	open_hw
	connect_hw_server
	open_hw_target

	current_hw_device [get_hw_devices xc7a35t_0]
	refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7a35t_0] 0]

	set_property PROBES.FILE {} [get_hw_devices xc7a35t_0]
	set_property FULL_PROBES.FILE {} [get_hw_devices xc7a35t_0]
	set_property PROGRAM.FILE {/home/cbjamo/projects/ROPS/hdl/xbuild/arty_test.runs/impl_1/top.bit} [get_hw_devices xc7a35t_0]
	program_hw_devices [get_hw_devices xc7a35t_0]

	refresh_hw_device [lindex [get_hw_devices xc7a35t_0] 0]

	exit


## References

ug835
ug892
