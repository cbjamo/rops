#!/usr/bin/python
import os
os.environ['KISYSMOD'] = '/usr/share/kicad/modules'

from skidl import *

@subcircuit
#def decouple_cap ( net1, net2, _value='47uF', _footprint='Capacitor_SMD:C_0805_2012Metric' ):
def decouple_cap ( net1, net2, size ):
    c = Part('Device', 'C', footprint=size[1], value=size[0])
    c[1,2] += net1, net2

@subcircuit
def pull_res ( net1, net2, _value='4.7k', _footprint='Resistor_SMD:R_0603_1608Metric' ):
    r = Part('Device', 'R', footprint=_footprint, value=_value)
    r[1,2] += net1, net2


### Power Supplies ###
gnd = Net('GND')
# 1V0
Vccint = Net('Vccint')
Vccbram = Net('Vccbram')
MGTAVcc = Net('MGTAVcc')
# 1V2
MGTAVtt = Net('MGTAVtt')
# 1v35
ramVdd = Net('ramVdd')
ramVddq = Net('ramVddq')
# 1V8
Vccaux = Net('Vccauc')
# 3V3
P3V3 = Net('3V3')
Vcco_0 = Net('Vcco_0')
Vcco_14 = Net('Vcco_14')

### FPGA ###
FPGA = Part('FPGA_Xilinx_Artix7.lib', 'XC7A35T-FGG484', footprint='Package_BGA:BGA-484_23.0x23.0mm_Layout22x22_P1.0mm')


# All grounds on the FPGA
FPGA['GND'] += gnd

# Decoupling caps for Artix Power
cap100uF = ['100uf', 'Capacitor_SMD:C_1210_3225Metric']
cap47uF = ['47uf', 'Capacitor_SMD:C_0805_2012Metric']
cap4_7uF = ['4.7uf', 'Capacitor_SMD:C_0402_1005Metric']
cap0_47uF = ['.47uf', 'Capacitor_SMD:C_0201_0603Metric']
cap0_1uF = ['0.1uf', 'Capacitor_SMD:C_0201_0603Metric']

# Main logic power
decouple_cap( Vccint, gnd, cap100uF )
for i in range(2):
    decouple_cap( Vccint, gnd, cap4_7uF )
for i in range(3):
    decouple_cap( Vccint, gnd, cap0_47uF )
FPGA['VCCINT'] += Vccint

# Ram interface power
decouple_cap( Vccbram, gnd, cap47uF )
decouple_cap( Vccbram, gnd, cap0_47uF )
FPGA['VCCBRAM'] += Vccbram

# Auxiliary logic power
decouple_cap( Vccaux, gnd, cap47uF )
for i in range(2):
    decouple_cap( Vccaux, gnd, cap4_7uF )
for i in range(5):
    decouple_cap( Vccaux, gnd, cap0_47uF )
FPGA['VCCAUX'] += Vccaux

# IO bank 0 power
decouple_cap( Vcco_0, gnd, cap47uF )
FPGA['VCCO_0'] += Vcco_0

def io_power( io_bank ):
    decouple_cap( Net.fetch('Vcco_'+io_bank), gnd, cap47uF )
    for i in range(2):
        decouple_cap( Net.fetch('Vcco_'+io_bank), gnd, cap4_7uF )
    for i in range(4):
        decouple_cap( Net.fetch('Vcco_'+io_bank), gnd, cap0_47uF )
    FPGA['VCCO_'+io_bank] += Net.fetch('Vcco_'+io_bank)

# IO bank power
io_power('14')
io_power('15')
io_power('16')
io_power('34')
io_power('35')

# GTP power
pull_res( MGTAVtt, FPGA['MGTRREF'], cap100uF )
decouple_cap( MGTAVtt, gnd, cap4_7uF )
for i in range(2):
    decouple_cap( MGTAVtt, gnd, cap0_1uF )
decouple_cap( MGTAVcc, gnd, cap4_7uF )
for i in range(2):
    decouple_cap( MGTAVcc, gnd, cap0_1uF )
FPGA['MGTAVCC'] += MGTAVcc
FPGA['MGTAVTT'] += MGTAVtt

### FPGA Config ###
SPI_Flash = Part( 'Memory_Flash', 'M25PX32-VMP', footprint='Package_DFN_QFN:DFN-8-1EP_6x5mm_P1.27mm_EP2x2mm')
JTAG_Port = Part( 'Connector_Generic', 'Conn_02x07_Odd_Even', value='JTAG', ref='JTAG_Conn',
        footprint='Connector_IDC:IDC-Header_2x07_P2.54mm_Vertical' )

TDI = Net('TDI')
TDO = Net('TDO')
TCK = Net('TCK')
TMS = Net('TMS')
JTAG = Bus('JTAG', TDI, TDO, TCK, TMS)

Program_B = Net('Program_B')
Init_B = Net('Init_B')
Conf_SPI_CS = Net('Conf_SPI_CS')
Conf_SPI_CLK = Net('Conf_SPI_CLK')
Conf_SPI_D = Bus('Conf_SPI_D', 4)
Conf_SPI = Bus('Conf_SPI', Conf_SPI_CS, Conf_SPI_CLK, Conf_SPI_D)

pull_res( Vcco_0, Program_B )
pull_res( Vcco_0, Init_B )
pull_res( Vcco_0, Conf_SPI_CS, _value='2.4k' )
pull_res( Vcco_0, Conf_SPI_D[2] )
pull_res( Vcco_0, Conf_SPI_D[3] )

FPGA['T[DCM][IOKS]'] += JTAG

JTAG_Port[1] += gnd
JTAG_Port[2] += Vcco_0 
JTAG_Port[3] += gnd  
JTAG_Port[4] += TMS
JTAG_Port[5] += gnd
JTAG_Port[6] += TCK
JTAG_Port[7] += gnd
JTAG_Port[8] += TDO
JTAG_Port[9] += gnd
JTAG_Port[10] += TDI

FPGA['PROGRAM_B'] += Program_B
FPGA['INIT_B'] += Init_B

FPGA['D0[0123]'] += Conf_SPI_D
FPGA['CCLK_0'] += Conf_SPI_CLK
FPGA['FCS_B'] += Conf_SPI_CS

FPGA['M[12]_0'] += gnd
FPGA['M0_0'] += Vcco_0
FPGA['PUDC_B'] += gnd
FPGA['VCCBATT'] += gnd

SPI_Flash['S#'] += Conf_SPI_CS
SPI_Flash['DQ0'] += Conf_SPI_D[0]
SPI_Flash['DQ1'] += Conf_SPI_D[1]
SPI_Flash['W#'] += Conf_SPI_D[2]
SPI_Flash['HOLD#'] += Conf_SPI_D[3]
SPI_Flash['C'] += Conf_SPI_CLK
SPI_Flash['VSS'] += gnd
SPI_Flash['VCC'] += Vcco_0

### RAM ###
RAM = Part( '../symbols/MT41K128M16.lib', 'MT41K128M16', footprint='Package_BGA:BGA-96_9.0x13.0mm_Layout2x3x16_P0.8mm')

# Power
RAM['VSS'] += gnd
RAM['VSSQ'] += gnd
RAM['VDD'] += ramVdd
RAM['VDDQ'] += ramVddq

#RAM['VREFCA'] += # ramVdd/2
#RAM['VREFQD'] += # ramVdd/2

### PCIe ###
PCI_Port = Part('/home/cbjamo/projects/ROPS/hardware/symbols/pcie-4x.lib', 'PCIe-4X', footprint='footprints:pcie-4x')

# Config/Power
PCI_Port['GND'] += gnd
#PCI_Port['3V3'] += P3V3
PCI_Port['TCK'] += TCK
PCI_Port['TDI'] += TDI
PCI_Port['TDO'] += TDO
PCI_Port['TMS'] += TMS
PCI_Port['TRST'] += Program_B #May be better choice for this
PCI_Port['PRSNT1'] += PCI_Port['B31']

REFCLKP = Net("REFCLK_P")
REFCLKN = Net("REFCLK_N")

REFCLKP += PCI_Port['REFCLK+'] 
REFCLKN += PCI_Port['REFCLK-'] 

REFCLKP += FPGA['MGTREFCLK0p']
REFCLKN += FPGA['MGTREFCLK0n']

# Diff pairs
HSO = Bus( 'HSO', Net('HSO0_P'), Net('HSO0_N'), Net('HSO1_P'), Net('HSO1_N'),
    Net('HSO2_P'), Net('HSO2_N'), Net('HSO3_P'), Net('HSO3_N') )
HSI = Bus( 'HSI', Net('HSI0_P'), Net('HSI0_N'), Net('HSI1_P'), Net('HSI1_N'),
    Net('HSI2_P'), Net('HSI2_N'), Net('HSI3_P'), Net('HSI3_N') )

HSO += PCI_Port['HSO[pn]\([0123]\)'] 
HSI += PCI_Port['HSI[pn]\([0123]\)'] 

HSO['HSO0_[PN]'] += FPGA['MGTPRX[PN][1]']
HSO['HSO1_[PN]'] += FPGA['MGTPRX[PN][2]']
HSO['HSO2_[PN]'] += FPGA['MGTPRX[PN][3]']
HSO['HSO3_[PN]'] += FPGA['MGTPRX[PN][0]']

HSI['HSI0_[PN]'] += FPGA['MGTPTX[PN][3]']
HSI['HSI1_[PN]'] += FPGA['MGTPTX[PN][2]']
HSI['HSI2_[PN]'] += FPGA['MGTPTX[PN][1]']
HSI['HSI3_[PN]'] += FPGA['MGTPTX[PN][0]']

### Expansion Headers ###
J1 = Part( 'Connector_Generic', 'Conn_02x40_Odd_Even', value='Bank 16', ref='J1',
        footprint='Connector_Samtec:MEC6-140-RA' )
J2 = Part( 'Connector_Generic', 'Conn_02x40_Odd_Even', value='Bank 15', ref='J2',
        footprint='Connector_Samtec:MEC6-140-RA' )
J3 = Part( 'Connector_Generic', 'Conn_02x40_Odd_Even', value='Bank 35', ref='J3',
        footprint='Connector_Samtec:MEC6-140-RA' )

# Outer Pins are connected to Power
for pin in [1,2,79,80]:
    J1[pin] += P3V3
    J2[pin] += P3V3
    J3[pin] += P3V3

# Pins between GPIO pairs connected to gnd
for pin in [3,4,9,10,15,16,21,22,27,28,33,34,39,40,41,42,47,48,53,54,59,60,65,66,71,72,77,78]:
    J1[pin] += gnd
    J2[pin] += gnd
    J3[pin] += gnd

# List of the pin #s connected to GPIO
# [5,6,7,8,11,12,13,14,17,18,19,20,23,24,25,26,29,30,31,32,35,36,37,38,43,44,45,46,47,49,50,51,52,55,56,57,58,61,62,63,64,67,68,69,70,73,74,75,76]

# Connect Banks 15 and 16 to J1 and J2. 
def diff_pair_connect( connector, connector_pin, fpga_pair, fpga_bank, swap=False ):
    p = Net.fetch( 'bank_' + fpga_bank + '_' + fpga_pair + '_P' )
    n = Net.fetch( 'bank_' + fpga_bank + '_' + fpga_pair + '_N' )

    p += FPGA[ 'l' + fpga_pair + 'p.*_' + fpga_bank ]
    n += FPGA[ 'l' + fpga_pair + 'n.*_' + fpga_bank ]

    if not swap:
        p += connector[connector_pin]
        n += connector[connector_pin+2]
    else:
        n += connector[connector_pin]
        p += connector[connector_pin+2]

# J1 mapping
diff_pair_connect( J1, 6, '24', '16' )
diff_pair_connect( J1, 12, '22', '16' )
diff_pair_connect( J1, 18, '20', '16' )
diff_pair_connect( J1, 24, '21', '16' )
diff_pair_connect( J1, 30, '16', '16' )
diff_pair_connect( J1, 36, '17', '16' )

diff_pair_connect( J1, 44, '11', '16', swap=True )
diff_pair_connect( J1, 50, '7', '16', swap=True  )
diff_pair_connect( J1, 56, '9', '16', swap=True  )
diff_pair_connect( J1, 62, '3', '16', swap=True  )
diff_pair_connect( J1, 68, '10', '16', swap=True  )
diff_pair_connect( J1, 74, '8', '16', swap=True  )

diff_pair_connect( J1, 5, '1', '16' )
diff_pair_connect( J1, 11, '18', '16' )
diff_pair_connect( J1, 17, '23', '16' )
diff_pair_connect( J1, 23, '14', '16' )
diff_pair_connect( J1, 29, '19', '16' )
diff_pair_connect( J1, 35, '15', '16' )

diff_pair_connect( J1, 43, '13', '16', swap=True )
diff_pair_connect( J1, 49, '12', '16', swap=True  )
diff_pair_connect( J1, 55, '2', '16', swap=True  )
diff_pair_connect( J1, 61, '5', '16', swap=True  )
diff_pair_connect( J1, 67, '6', '16', swap=True  )
diff_pair_connect( J1, 73, '4', '16', swap=True  )

# J2 mapping
diff_pair_connect( J2, 6, '13', '15' )
diff_pair_connect( J2, 12, '21', '15' )
diff_pair_connect( J2, 18, '12', '15' )
diff_pair_connect( J2, 24, '6', '15' )
diff_pair_connect( J2, 30, '4', '15' )
diff_pair_connect( J2, 36, '18', '15' )

diff_pair_connect( J2, 44, '15', '15', swap=True )
diff_pair_connect( J2, 50, '10', '15', swap=True  )
diff_pair_connect( J2, 56, '9', '15', swap=True  )
diff_pair_connect( J2, 62, '11', '15', swap=True  )
diff_pair_connect( J2, 68, '7', '15', swap=True  )
diff_pair_connect( J2, 74, '8', '15', swap=True  )

diff_pair_connect( J2, 5, '17', '15' )
diff_pair_connect( J2, 11, '19', '15' )
diff_pair_connect( J2, 17, '22', '15' )
diff_pair_connect( J2, 23, '23', '15' )
diff_pair_connect( J2, 29, '3', '15' )
diff_pair_connect( J2, 35, '5', '15' )

diff_pair_connect( J2, 43, '1', '15', swap=True )
diff_pair_connect( J2, 49, '2', '15', swap=True  )
diff_pair_connect( J2, 55, '20', '15', swap=True  )
diff_pair_connect( J2, 61, '24', '15', swap=True  )
diff_pair_connect( J2, 67, '16', '15', swap=True  )
diff_pair_connect( J2, 73, '14', '15', swap=True  )

# J3 mapping
diff_pair_connect( J3, 6, '21', '35' )
diff_pair_connect( J3, 12, '24', '35' )
diff_pair_connect( J3, 18, '19', '35' )
diff_pair_connect( J3, 24, '23', '35' )
diff_pair_connect( J3, 30, '18', '35' )
diff_pair_connect( J3, 36, '17', '35' )

diff_pair_connect( J3, 44, '13', '35', swap=True )
diff_pair_connect( J3, 50, '10', '35', swap=True  )
diff_pair_connect( J3, 56, '11', '35', swap=True  )
diff_pair_connect( J3, 62, '12', '35', swap=True  )
diff_pair_connect( J3, 68, '4', '35', swap=True  )
diff_pair_connect( J3, 74, '2', '35', swap=True  )

diff_pair_connect( J3, 5, '20', '35' )
diff_pair_connect( J3, 11, '22', '35' )
diff_pair_connect( J3, 17, '16', '35' )
diff_pair_connect( J3, 23, '15', '35' )
diff_pair_connect( J3, 29, '14', '35' )
diff_pair_connect( J3, 35, '7', '35' )

diff_pair_connect( J3, 43, '9', '35', swap=True )
diff_pair_connect( J3, 49, '8', '35', swap=True  )
diff_pair_connect( J3, 55, '5', '35', swap=True  )
diff_pair_connect( J3, 61, '6', '35', swap=True  )
diff_pair_connect( J3, 67, '3', '35', swap=True  )
diff_pair_connect( J3, 73, '1', '35', swap=True  )


# MicroSD card
#sdcard = Part( 'asdf', 'asdf', value='MicroSD', ref='C1',
#        footprint='asdf' )

# 

generate_netlist()
