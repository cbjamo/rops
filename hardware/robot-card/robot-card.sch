EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5B54D3A0
P 3550 2700
F 0 "J?" H 3630 2692 50  0000 L CNN
F 1 "Conn_01x02" H 3630 2601 50  0000 L CNN
F 2 "" H 3550 2700 50  0001 C CNN
F 3 "~" H 3550 2700 50  0001 C CNN
	1    3550 2700
	1    0    0    -1  
$EndComp
$Comp
L neo-m8t:NEO-M8T U?
U 1 1 5B54D556
P 2500 2500
F 0 "U?" H 2500 3437 60  0000 C CNN
F 1 "NEO-M8T" H 2500 3331 60  0000 C CNN
F 2 "" H 1700 3050 60  0001 C CNN
F 3 "" H 1700 3050 60  0001 C CNN
	1    2500 2500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
