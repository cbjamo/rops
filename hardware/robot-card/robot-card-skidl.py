#!/usr/bin/python
import os
os.environ['KISYSMOD'] = '/usr/share/kicad/modules'

from skidl import *

@subcircuit
#def decouple_cap ( net1, net2, _value='47uF', _footprint='Capacitor_SMD:C_0805_2012Metric' ):
def decouple_cap ( net1, net2, size ):
    c = Part('Device', 'C', footprint=size[1], value=size[0])
    c[1,2] += net1, net2

@subcircuit
def pull_res ( net1, net2, _value='4.7k', _footprint='Resistor_SMD:R_0603_1608Metric' ):
    r = Part('Device', 'R', footprint=_footprint, value=_value)
    r[1,2] += net1, net2

### Power Supplies ###
gnd = Net('GND')
agnd = Net('AGND')
# 3V3
P3V3 = Net('3V3')

### SPI Bus ###
MOSI = Net('MOSI')
MISO = Net('MISO')
SCLK = Net('SCLK')

### GPS ###
neo_m8t = Part('/home/cbjamo/projects/ROPS/hardware/symbols/neo-m8t.lib', 'neo-m8t', ref='m8t',
        footprint='footprints:NEO-M8')
sma_10mhz = Part('Connector_Generic', 'Conn_01x02', ref='10M',
        footprint='footprints:CON-SMA-EDGE')

neo_m8t_cs = Net('gps_cs')
neo_m8t_reset = Net('gps_reset')

# Power/Config
neo_m8t['VCC'] += P3V3
neo_m8t['GND'] += gnd
neo_m8t['AGND'] += agnd
neo_m8t['D_SEL'] += gnd
decouple_cap( neo_m8t['VCC'], gnd, ('10nF', 'Capacitor_SMD:C_0805_2012Metric') )

# SPI
neo_m8t['MOSI'] += MOSI
neo_m8t['MISO'] += MISO
neo_m8t['SCL'] += SCLK
neo_m8t['SDA/CS'] += neo_m8t_cs

neo_m8t['RESET_N'] += neo_m8t_reset

# Ant
sma_ant = Part('Connector_Generic', 'Conn_01x02', ref='ANT',
        footprint='footprints:CON-SMA-EDGE')
ant_ind_rf_block = Part('Device', 'L', footprint='Inductor_SMD:L_0805_2012Metric', value='27nH')
ant_cap_dc_block = Part('Device', 'C', footprint='Capacitor_SMD:C_0805_2012Metric', value='47pF')
ant_cap_active_decouple = Part('Device', 'C', footprint='Capacitor_SMD:C_0805_2012Metric', value='10nF')
ant_res_active_bias = Part('Device', 'R', footprint='Resistor_SMD:R_0805_2012Metric', value='10')
ant_active_jumper = Part('Jumper', 'SolderJumper_2_Bridged', footprint='Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm' )

ant_cap_dc_block[1,2] += neo_m8t['RF_IN'], sma_ant[1]
ant_ind_rf_block[1] += sma_ant[1] 
ant_ind_rf_block[2] += ant_cap_active_decouple[1]
ant_ind_rf_block[2] += ant_res_active_bias[1]
ant_cap_active_decouple[2] += gnd
ant_res_active_bias[2] += ant_active_jumper[1]
ant_active_jumper[2] += neo_m8t['VCC_RF']
sma_ant[2] += agnd

clock_cap_dc_block = Part('Device', 'C', footprint='Capacitor_SMD:C_0805_2012Metric', value='47pF')
clock_cap_dc_block[1,2] += neo_m8t['TIMEPULSE1'], sma_10mhz[1]
sma_10mhz[2] += gnd

### IMU ###
imu = Part('/home/cbjamo/projects/ROPS/hardware/symbols/lsm9ds1.lib', 'lsm9ds1', ref='IMU',
        footprint='footprints:LGA-24_3x3.5mm_LSM9DS1')

### Baro ###
baro = Part('/home/cbjamo/projects/ROPS/hardware/symbols/lps35hw.lib', 'lps35hw', ref='Baro',
        footprint='footprints:CCLGA_10L')

### Expansion Headers ###
edge = Part('/home/cbjamo/projects/ROPS/hardware/symbols/rops-card.lib', 'rops-card', ref='edge',
        footprint='footprints:MEC6-140-RA-EDGE')

edge['3v3'] += P3V3
edge['GND'] += gnd

edge['MRCC0_P'] += neo_m8t['TIMEPULSE1']
edge['MRCC0_N'] += gnd

edge['MRCC1_P'] += neo_m8t['TP2/SAFEBOOT_N']
edge['MRCC1_N'] += gnd

generate_netlist()
