# ROPS 

Robot on a PCI-e Stick (ROPS) is a project to provide a low-cost solution to some of the common problems that ROS robots face. 

All robots need sensors, computation and actuation.
This project seeks to simplify, streamline, and optimize the connection between those systems.
Off the shelf motherboards do not provide a good way to interface with sensors and actuators.
ROPS aims to combine the IO flexability of an FPGA, low latency and high speed of PCI-e, and some basic sensors that are useful to all mobile robots.

