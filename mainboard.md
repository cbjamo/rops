# Hardware

## ICs

* Xilinx Artix-7 XC7A35T 
	* FGG484 23x23mm, 1mm ball pitch
	* 4x GTP, 5x 50pin HR IO
	* 52.99$ @ 1

* DDR3 Ram
	* Micron MT41K128M16
		* 2Gb (128Mx16)
		* 96FBGA, 9x13mm, 0.8mm ball pitch 
		* 10.53$ @ 1, 7.3$ @ 2000
	* Micron MT41K512M16
		* 8Gb (512Mx16)
		* 96FBGA, 9x13mm, 0.8mm ball pitch 
		* 32.86$ @ 1, 31.2$ @ 2000

* Quad-spi Flash
	* ISSI IS25WP032D
		* 1.65-1.95V
		* 32Mb (4M x 8)
		* 8-WSON 6x5mm, 1.27mm pitch
		* 1.82$ @ 1, 1.43$ @ 100
	* ISSI IS25LP032D
		* 2.3-3.6V
		* 32Mb (4M x 8)
		* 8-WSON 6x5, 1.27mm pitch
		* 1.69$ @ 1, 1.337$ @ 100

* PMIC
	* Dialog Semi DA9062
		* 4 Buck
		* 4 LDO
		* 2.21$ @ 1, 1.74$ @ 100 (Avnet EU)

* UART/JTAG to USB
	* FTDI FT2232H
		* 56-VFQFN EP 8x8mm, 0.5mm pitch
		* 5.8$ @ 1, 4.8$ @ 100
	* Cypress FX2
		* 56-VFQFN EP 8x8mm, 0.5mm pitch
		* 12.12$ @ 1, 9.36$ @ 100

* Main Clock
	* Microchip DSC6102CI2A-100.0000T
		* 1.71-3.63V
		* 100Mhz, 25ppm
		* 4-SMD 2.3x2.5mm
		* 0.92$ @ 1, 0.80$ @ 50

* ADC Vref
	* TI REF3012AIDBZR 
		* 1.25V Ref
		* 1.8-5.5V In
		* 1.43$ @ 1, 1$ @ 100

## I/O

* Connectors
	* PCIe to host PC
	* USB-C to uart
	* MicroSD card slot
	* .1in jtag header
	
* MEC6-140-02-L-D-RA1
	* 80 pin, 28.6x9.1mm, 0.635mm pitch
	* 2x external
	* 1x internal
	* 9.48$ @ 1, 7.63$ @ 100

## Power

* DA9062
	* Buck1 - 1.0V, 2.5A - Core, BRam
	* Buck2 - 1.35V, 2.5A - DDRVdd/q
	* Buck3 - 1.8V, 2A - Aux
	* Buck4 - 0.675, 1.5A - DDRVtt
	* LDO1 - 1.8V, 100mA - ADCVcc
	* LDO2 - 0.9-3.6V, 300mA - Not Used
	* LDO3 - 1.0V, 300mA - GTPVcc
	* LDO4 - 1.2V, 300mA - GTPVtt

## Dimensions

* PCIe Max - 107mm x 312mm
* PCIe Half - 64.41mm x 167.64mm
