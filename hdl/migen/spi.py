from migen import *

class spi_transact(Module):
    def __init__(self, reg_width=8):
        self.rstn = Signal()
        self.t_start = Signal()
        self.d_in = Signal(reg_width)
        self.d_out = Signal(reg_width)
        self.t_size = Signal(max=8)

        self.cpol = Signal()
        self.cpha = Signal()

        self.miso = Signal()
        self.mosi = Signal()
        self.cs = Signal()
        self.sclk = Signal()

        ###

        self.shift = Signal(reg_width)
        self.count = Signal(max=reg_width)

        fsm = FSM(reset_state="reset")
        self.submodules += fsm

        fsm.act("reset",
                self.cs.eq(1),
                self.shift.eq(0),
                self.count.eq(0),
                If(t_start,
                    NextState("load")
                    )
                )
        fsm.act("load",
                self.cs.eq(0),
                self.shift.eq(self.d_in),
                self.count.eq(self.t_size),
                NextState("shift")
                )
        fsm.act("shift",
                self.cs.eq(0),
                NextValue(self.shift, self.shift ), #shifted 1
                NextValue(self.count, self.count - 1),
                If(self.count == 0,
                    NextState("unload")
                    )
                )
        fsm.act("unload",
                self.cs.eq(0),
                self.d_out.eq(self.shift),
                NextState("idle")
                )
        fsm.act("idle",
                self.cs.eq(1),
                If(t_start,
                    NextState("load")
                    )
                )
