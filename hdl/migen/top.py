from migen import *
from migen.build.platforms import arty_a7

# The platform has all the info about what FPGA is being used,
# how hardware on the PCB is connected to the FPGA,
# and how what tool to use to build the bitstream.
plat = arty_a7.Platform()

# Here we just request an led so we can blink it.
led = plat.request("user_led")

pmod = plat.request("pmoda")

# This module just counts a register from 0 to 2^25-1 
# You'll note that we don't specify a clock, this is because the platform has a default clock, in the case of the Arty, it's a 100Mhz.
# So our counter will overflow (and change the led state) at about 3hz, a fine blinky indeed.
m = Module()
counter = Signal(26)
m.comb += led.eq(counter[25])
m.sync += counter.eq(counter + 1)

# Make vivado do the thing.
plat.build(m)
