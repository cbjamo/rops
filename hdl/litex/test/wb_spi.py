#!/usr/bin/python

import time
from litex.soc.tools.remote import RemoteClient

wb = RemoteClient()
wb.open()

def config_spi( cpol, cpha, spi_clk_rate ):
    clk_div = int(100e6/spi_clk_rate) - 2
    wb.regs.pmoda_spi_config.write( cpol<<4 | cpha<<5 | clk_div<<16 | clk_div<<24 )

def spi_t( data, count ):
    wb.regs.pmoda_spi_xfer.write( (2**16)-1 | count<<16 )
    wb.regs.pmoda_spi_mosi_data.write( data<<(32-count) )
    wb.regs.pmoda_spi_start.write( 1 )
    while( wb.regs.pmoda_spi_active.read() ):
        pass
    return wb.regs.pmoda_spi_miso_data.read()

def rw_reg( write, adr, data ):
    return spi_t( (~write<<15 | adr<<8 | data ), 16 )

def hprint( dat ):
    print( format( dat, '0x' ) )

RW_BIT = 15
ADR_BIT = 8
WHO_AM_I = 0x0F
OUT_TEMP_L = 0x15
OUT_TEMP_H = 0x16
CTRL_REG5_XL = 0x1F
CTRL_REG9 = 0x23
OUT_X_XL_L = 0x28
OUT_X_XL_H = 0x29
OUT_Y_XL_L = 0x2A
OUT_Y_XL_H = 0x2B
OUT_Z_XL_L = 0x2C
OUT_Z_XL_H = 0x2D
STATUS_REG = 0x17
FIFO_CTRL = 0x2E

config_spi( 0, 1, 5e6 )

hprint( rw_reg( 0, WHO_AM_I, 0) )
#hprint( rw_reg( 1, CTRL_REG5_XL, 7<<3 ) )
#hprint( rw_reg( 0, CTRL_REG5_XL, 0 ) )

hprint( rw_reg( 1, FIFO_CTRL, 6<<5 ) )
#hprint( rw_reg( 0, FIFO_CTRL, 0 ) )

hprint( rw_reg( 0, STATUS_REG, 0) )
hprint( spi_t( (1<<23 | OUT_TEMP_L<<16), 24 ) )

wb.close()
