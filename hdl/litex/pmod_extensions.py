#!/usr/bin/env python3
from migen import *
from litex.build.generic_platform import *

def PMODType1( pmod="pmoda", iostandard="LVCMOS33" ):

    type1_pmod = [( pmod + "_type1", 0,
            Subsignal("IO1", Pins(pmod + ":0")),
            Subsignal("IO2", Pins(pmod + ":1")),
            Subsignal("IO3", Pins(pmod + ":2")),
            Subsignal("IO4", Pins(pmod + ":3")),
            IOStandard(iostandard)) ]

    return type1_pmod

def PMODType2( pmod="pmoda", iostandard="LVCMOS33" ):

    type2_pmod = [( pmod + "_type2", 0,
            Subsignal("cs_n", Pins(pmod + ":0")),
            Subsignal("modi", Pins(pmod + ":1")),
            Subsignal("miso", Pins(pmod + ":2")),
            Subsignal("clk", Pins(pmod + ":3")),
            IOStandard(iostandard)) ]

    return type2_pmod

def PMODType2A( pmod="pmoda", iostandard="LVCMOS33" ):

    type2a_pmod = [( pmod + "_type2a", 0,
            Subsignal("cs_n", Pins(pmod + ":0")),
            Subsignal("mosi", Pins(pmod + ":1")),
            Subsignal("miso", Pins(pmod + ":2")),
            Subsignal("clk", Pins(pmod + ":3")),
            Subsignal("INT", Pins(pmod + ":4")),
            Subsignal("RESET", Pins(pmod + ":5")),
            Subsignal("NS0", Pins(pmod + ":6")),
            Subsignal("NS1", Pins(pmod + ":7")),
            IOStandard(iostandard)) ]

    return type2a_pmod

def PMODType3( pmod="pmoda", iostandard="LVCMOS33" ):

    type3_pmod = [( pmod + "_type3", 0,
            Subsignal("CTS", Pins(pmod + ":0")),
            Subsignal("RTS", Pins(pmod + ":1")),
            Subsignal("RXD", Pins(pmod + ":2")),
            Subsignal("TXD", Pins(pmod + ":3")),
            IOStandard(iostandard)) ]

    return type3_pmod

def PMODType4( pmod="pmoda", iostandard="LVCMOS33" ):

    type4_pmod = [( pmod + "_type4", 0,
            Subsignal("CTS", Pins(pmod + ":0")),
            Subsignal("TXD", Pins(pmod + ":1")),
            Subsignal("RXD", Pins(pmod + ":2")),
            Subsignal("RTS", Pins(pmod + ":3")),
            IOStandard(iostandard)) ]

    return type4_pmod

def PMODType4a( pmod="pmoda", iostandard="LVCMOS33" ):

    type4a_pmod = [( pmod + "_type4a", 0,
            Subsignal("CTS", Pins(pmod + ":0")),
            Subsignal("TXD", Pins(pmod + ":1")),
            Subsignal("RXD", Pins(pmod + ":2")),
            Subsignal("RTS", Pins(pmod + ":3")),
            Subsignal("INT", Pins(pmod + ":4")),
            Subsignal("RESET", Pins(pmod + ":5")),
            Subsignal("NS0", Pins(pmod + ":6")),
            Subsignal("NS1", Pins(pmod + ":7")),
            IOStandard(iostandard)) ]

    return type4a_pmod

def PMODType5( pmod="pmoda", iostandard="LVCMOS33" ):

    type5_pmod = [( pmod + "_type5", 0,
            Subsignal("DIR", Pins(pmod + ":0")),
            Subsignal("EN", Pins(pmod + ":1")),
            Subsignal("SA", Pins(pmod + ":2")),
            Subsignal("SB", Pins(pmod + ":3")),
            IOStandard(iostandard)) ]

    return type5_pmod

def PMODType6( pmod="pmoda", iostandard="LVCMOS33" ):

    type6_pmod = [( pmod + "_type6", 0,
            Subsignal("DIR1", Pins(pmod + ":0")),
            Subsignal("EN1", Pins(pmod + ":1")),
            Subsignal("DIR2", Pins(pmod + ":2")),
            Subsignal("EN2", Pins(pmod + ":3")),
            IOStandard(iostandard)) ]

    return type6_pmod

