`timescale 1ms/1ms

module leading_one_detector_test();
	reg [7:0] d;
	wire [2:0] y;
	wire z;

	leading_one_detector
	#() DUT
	(
		.d(d),
		.y(y),
		.z(z)
	);

	integer i;

	initial
	begin
		$monitor("d:%d, y:%b, z:%b", d, y, z);
		for( i=0; i < 256; i=i+1 ) begin
			d <= i;
			#1;
		end
		$finish;
	end
endmodule

module leading_one_detector
(
	input [7:0] d,
	output reg [2:0] y,
	output wire z
);
	assign z = ~|d;
/*
	assign y[0] = 2**1 <= d && d < 2**2 || 2**3 <= d && d < 2**4 || 2**5 <= d && d < 2**6 || 2**7 <= d && d < 2**8;
	assign y[1] = 2**2 <= d && d < 2**4 || 2**6 <= d && d < 2**8;
	assign y[2] = 2**4 <= d && d < 2**8;
*/
	always @ * begin
		casez (d)
			8'b1???_???? : y = 7;
			8'b01??_???? : y = 6;
			8'b001?_???? : y = 5;
			8'b0001_???? : y = 4;
			8'b0000_1??? : y = 3;
			8'b0000_01?? : y = 2;
			8'b0000_001? : y = 1;
			8'b0000_0001 : y = 0;
			8'b0000_0000 : y = 0;
		endcase
	end
endmodule
