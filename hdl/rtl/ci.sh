while true;
	do 
		inotifywait -e close_write $1
		iverilog -o main $1 && \
			vvp main -lxt2 && \
			gconftool-2 --type string --set /com.geda.gtkwave/0/reload 0
done
