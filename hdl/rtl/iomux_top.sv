module top
(
	input uart_txd_in,
	output uart_rxd_out,

	//input SDO, INT, DRDY_M,
	//output SDI, SCK, CS_AG, CS_M, CS_ALT,

	input CLK100MHZ,
	input ck_rst,
	input [3:0] sw,
	input [3:0] btn,
	output [3:0] led,
	inout [3:0] jc,
	inout [3:0] jd
);
	assign uart_rxd_out = uart_txd_in;

	wb_iomux
	#(
		.PIN_WIDTH( 4 ),
		.PORT_WIDTH( 4 )
	)
	wb_iomux_inst
	(
		.pins( jd ),
		.ports( jc )
	);

endmodule
