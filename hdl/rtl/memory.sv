`timescale 1ms/1ms

module memory_test();

	parameter bits = 8;

	reg clk;
	reg write;
	reg [bits-1:0] addr;
	reg [bits-1:0] in;
	wire [bits-1:0] out;

	memory
	#( 
		.word(bits),
		.depth(256)	
	) DUT
	(
		.clk(clk),
		.out(out),
		.in(in),
		.write(write),
		.addr(addr)
	);

	initial
	begin
		clk = 0;
	end

	always
		#5 clk = !clk;

	integer i;

	initial
	begin
		$monitor("clk:%b, A:%b, DI:%b, DO:%b", clk, addr, in, out);

		write = 1'b1;
		for( i = 0; i < (2**bits); i++ )
		begin
			addr = i;
			in = i;
			#10;
		end
		in = 8'h00;
		write = 1'b0;
		for( i = 0; i < (2**bits); i++ )
		begin
			addr = i;
			#10;
		end
		$finish;
	end

endmodule

// m-bit counter
module memory
#(
	parameter word=8,
	parameter depth=8
)
(
	input clk,
	input write,
	input [word-1:0] addr,
	input [word-1:-0] in,
	output reg[word-1:0] out
);

	reg [word-1:0] mem [0:depth-1];

	always @ (posedge clk)
	begin
		if (write)
			mem[addr] <= in;
		else
			out <= mem[addr];
	end
endmodule

