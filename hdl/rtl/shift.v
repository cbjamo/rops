module shift_reg_test();

	parameter bits = 32;

	reg clk;
	reg en;
	reg dir;
	reg rstn;
	reg load_en;
	reg [bits-1:0] in;
	wire [bits-1:0] out;

	shift_reg
	#(
		.MSB(bits)
	) DUT
	(
		.clk(clk),
		.en(en),
		.dir(dir),
		.rstn(rstn),
		.load_en(load_en),
		.in(in),
		.out(out)
	);

	always
		#5 clk = !clk;

	initial
	begin
		#330 $finish;
	end

	initial
	begin
		clk = 0;
		en = 0;
		dir = 0;
		rstn = 0;
		load_en = 0;
		in = 0;
	end

	initial
	begin
		$display("\t\ttime,\tclk,\treset,\tenable,\tout");
		$monitor("%d,\t%b,\t%b,\t%b,\t%b",$time, clk,rstn,en,out);
	end

	initial
	begin
		#1
		en = 1;
		rstn = 1;
		load_en = 1;
		dir = 1;
		in = 32'hDEADBEEF;
		#10
		load_en = 0;
		in = 0;
		#50
		in[0] = 1;
		#50
		dir = 0;
		in[0] = 0;
		#50
		dir = 1;
	end

endmodule

// m-bit shift register
module shift_reg
#(
	parameter MSB=8
)
(
	input clk,
	input en,
	input dir,
	input rstn,
	input load_en,
	input [MSB-1:0] in,
	output reg [MSB-1:0] out
);

	always @ (posedge clk)
	begin
		if (!rstn)
			out <= 0;
		else if (en)
			if (load_en)
				out <= in;
			else
				case (dir)
					0 :  out <= {out[MSB-2:0], in[0]};
					1 :  out <= {in[0], out[MSB-1:1]};
				endcase
		else
		out <= out;
	end
endmodule

