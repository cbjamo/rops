module wb_syscon
#(
	parameter REF_CLK_HZ = 100000000,
	parameter WB_CLK_HZ = 400000000 // TODO use this to calculate mult/div params in MMCM
)
(
	input ref_clk_i,
	input m_rst_n_i,

	output wire wb_clk_o,
	output wire wb_rst_o
);
	localparam CLKIN_PERIOD = ((1 / REF_CLK_HZ) * 10^9); // Convert clock freq in hz to period in ns

	assign wb_rst_o = m_rst_n_i && eos;

	wire eos;
	wire CLKFB;


	// STARTUPE2: STARTUP Block 
	// 7 Series 
	// Xilinx HDL Libraries Guide, version 14.2 

	STARTUPE2 #( 
		.PROG_USR("FALSE"), 	// Activate program event security feature. Requires encrypted bitstreams.
		.SIM_CCLK_FREQ(0.0) 	// Set the Configuration Clock Frequency(ns) for simulation. )
	)
	STARTUPE2_inst ( 
		.CFGCLK(), 		// 1-bit output: Configuration main clock output 
		.CFGMCLK(), 	// 1-bit output: Configuration internal oscillator clock output 
		.EOS( eos ),	// 1-bit output: Active high output signal indicating the End Of Startup. 
		.PREQ(), 		// 1-bit output: PROGRAM request to fabric output 
		.CLK(), 		// 1-bit input: User start-up clock input 
		.GSR(), 		// 1-bit input: Global Set/Reset input (GSR cannot be used for the port name) 
		.GTS(), 		// 1-bit input: Global 3-state input (GTS cannot be used for the port name) 
		.KEYCLEARB(), 	// 1-bit input: Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM)
		.PACK(), 		// 1-bit input: PROGRAM acknowledge input 
		.USRCCLKO(), 	// 1-bit input: User CCLK input 
		.USRCCLKTS(), 	// 1-bit input: User CCLK 3-state enable input 
		.USRDONEO(), 	// 1-bit input: User DONE pin output control 
		.USRDONETS() 	// 1-bit input: User DONE 3-state enable output
	);


	// PLLE2_BASE: Base Phase Locked Loop (PLL)
	//						 Artix-7
	// Xilinx HDL Language Template, version 2016.2

	MMCME2_BASE #(
		.BANDWIDTH("OPTIMIZED"),			// OPTIMIZED, HIGH, LOW
		.CLKFBOUT_MULT_F(10),				// Multiply value for all CLKOUT, (2-64)
		.CLKFBOUT_PHASE(0.0),		 		// Phase offset in degrees of CLKFB, (-360.000-360.000).
		.CLKIN1_PERIOD(CLKIN_PERIOD),		// Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
		// CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
		.CLKOUT0_DIVIDE_F(2.5),
		// CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
		.CLKOUT0_DUTY_CYCLE(0.5),
		// CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
		.CLKOUT0_PHASE(0.0),
		.DIVCLK_DIVIDE(5),					// Master division value, (1-56)
		.REF_JITTER1(0.1),					// Reference input jitter in UI, (0.000-0.999).
		.STARTUP_WAIT("FALSE")				// Delay DONE until PLL Locks, ("TRUE"/"FALSE")
	)
	MMCME2_BASE_inst (
		// Clock Outputs: 1-bit (each) output: User configurable clock outputs
		.CLKOUT0(wb_clk_o),	 				// 1-bit output: CLKOUT0
		.CLKOUT0B(),	 					// 1-bit output: CLKOUT0
		.CLKOUT1(),	 						// 1-bit output: CLKOUT1
		.CLKOUT1B(),	 					// 1-bit output: CLKOUT1
		.CLKOUT2(),	 						// 1-bit output: CLKOUT1
		.CLKOUT2B(),	 					// 1-bit output: CLKOUT1
		.CLKOUT3(),	 						// 1-bit output: CLKOUT1
		.CLKOUT3B(),	 					// 1-bit output: CLKOUT1
		.CLKOUT4(),	 						// 1-bit output: CLKOUT1
		.CLKOUT5(),	 						// 1-bit output: CLKOUT1
		.CLKOUT6(),	 						// 1-bit output: CLKOUT1
		// Feedback Clocks: 1-bit (each) output: Clock feedback ports
		.CLKFBOUT(CLKFB), 					// 1-bit output: Feedback clock
		.CLKFBOUTB(), 						// 1-bit output: Feedback clock
		.LOCKED(LOCKED),		 			// 1-bit output: LOCK
		.CLKIN1(ref_clk_i),		 			// 1-bit input: Input clock
		// Control Ports: 1-bit (each) input: PLL control ports
		.PWRDWN(1'b0),		 				// 1-bit input: Power-down
		.RST(m_rst_n_i),				 	// 1-bit input: Reset
		// Feedback Clocks: 1-bit (each) input: Clock feedback ports
		.CLKFBIN(CLKFB)						// 1-bit input: Feedback clock
	);

endmodule
