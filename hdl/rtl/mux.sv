module mux
#(
	parameter BITS = 8,
	parameter INPUTS = 3
)
(
	input [$clog2(INPUTS)-1:0] select_i,
	inout oe_i,
	input [BITS-1:0] dat_i [INPUTS-1:0],
	output reg [BITS-1:0] dat_o
);
	always_comb
	begin
		dat_o = 'z;
		for (int i = 0; i < INPUTS; i++ )
		begin
			if ( (select_i == i) && oe_i )
				dat_o = dat_i[i];
		end
	end
endmodule

module demux
#(
	parameter BITS = 8,
	parameter OUTPUTS = 3
)
(
	input [$clog2(OUTPUTS)-1:0] select_i,
	input oe_i,
	output reg [BITS-1:0] dat_o [OUTPUTS-1:0],
	input [BITS-1:0] dat_i
);
	always_comb
	begin
		for (int i = 0; i < OUTPUTS; i++ )
		begin
			dat_o[i] = 'z;
			if ( (select_i == i) && oe_i )
				dat_o[i] = dat_i;
		end
	end
endmodule


module onehot_mux
#(
	parameter BITS = 8,
	parameter INPUTS = 3
)
(
	input [INPUTS-1:0] select_i,
	input oe_i,
	input [BITS-1:0] dat_i [INPUTS-1:0],
	output reg [BITS-1:0] dat_o
);
	always_comb
	begin
		dat_o = 'z;
		for (int i = 0; i < INPUTS; i++ )
		begin
			if ( (select_i == (1 << i) ) && oe_i )
				dat_o = dat_i[i];
		end
	end
endmodule

module bidir_mux
#(
	parameter BITS = 8,
	parameter INOUTS = 3
)
(
	input dir_i,
	input [$clog2(INOUTS)-1:0] select_i,
	input oe_i,
	inout [BITS-1:0] dat_a [INOUTS-1:0],
	inout [BITS-1:0] dat_b
);
	mux #( .BITS( BITS ), .INPUTS(INOUTS) ) mux_inst ( .select_i( select_i ), .oe_i( dir_i && oe_i ), .dat_i( dat_a ), .dat_o( dat_b ) );
	demux #( .BITS( BITS ), .OUTPUTS(INOUTS) ) demux_inst ( .select_i( select_i ), .oe_i( ~dir_i && oe_i ), .dat_i( dat_b ), .dat_o( dat_a ) );

endmodule
