`timescale 1ms/1ms

module counter_test();

	parameter bits = 3;

	reg en;
	reg clk;
	reg rstn;
	reg dir;
	reg load_en;
	reg reload;
	reg [bits-1:0] d;
	wire [bits-1:0] out;

	counter
	#( .MSB(bits) ) DUT
	(
		.en(en),
		.clk(clk),
		.out(out),
		.dir(dir),
		.d(d),
		.reload(reload),
		.load_en(load_en),
		.rstn(rstn)
	);

	initial
	begin
		en = 1;
		clk = 0;
		rstn = 0;
		dir = 1;
		d = 7;
		#2;
		rstn = 1;
	end

	always
		#1 clk = !clk;

	initial
	begin
		#2;
		load_en = 1;
		#2;
		load_en = 0;
		dir = 0;
		#16;
		reload = 1;
		#2;
		reload = 0;
		#16;
		$finish;
	end

	initial
	begin
		$display("\t\ttime,\tclk,\treset,\tenable,\tcount,\tload,\td");
		$monitor("%d,\t%b,\t%b,\t%b,\t%d\t%b,\t%d",$time, clk,rstn,en,out,load_en,d);
	end

endmodule

// m-bit counter
module counter
#(
	parameter MSB=8
)
(
	input en,
	input clk,
	input rstn,
	input dir,
	input load_en,
	input reload,
	input [MSB-1:0] d,
	output reg[MSB-1:0] out
);
	reg [MSB-1:0] d_bak;

	always @ (posedge clk)
	begin
		if (! rstn)
			out <= 0;
		else if (en)
			if (load_en)
				out <= d;
			else
				if (dir)
					out <= out + 1;
				else
					out <= out - 1;
	end
endmodule

