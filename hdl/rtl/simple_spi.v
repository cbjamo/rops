/// SPI
module axi_spi
#(
	parameter DATA_WIDTH = 8
)
(

	input  wire clk,
	input  wire rst,

	/*
	 * AXI input
	 */
	input  wire [DATA_WIDTH-1:0] input_axis_tdata,
	input  wire input_axis_tvalid,
	//output wire input_axis_tready,

	/*
	 * AXI output
	 */
	output wire [DATA_WIDTH-1:0] output_axis_tdata,
	output wire output_axis_tvalid,
	//input  wire output_axis_tready,

	/*
	 * UART interface
	 */
	input  wire miso,
	output wire mosi,
	output wire spi_clk,
	output wire cs,

	/*
	 * Status
	 */
	output wire busy

	/*
	 * Configuration
	 */
	//input  wire [15:0] prescale
);
	localparam idle = 0, load = 1, transact = 2, unload = 3;

	reg miso_d;
	reg [1:0] state;
	reg [$clog2(DATA_WIDTH):0] count;
	reg [DATA_WIDTH-1:0] shift;
	reg [DATA_WIDTH-1:0] output_axis_tvalid_reg;
	reg [DATA_WIDTH-1:0] output_axis_tdata_reg;

	assign busy = ~cs;
	assign mosi = ( ~cs ) ? shift[DATA_WIDTH-1] : 1'bz;
	assign spi_clk = ( state == transact ) ? clk : 1'b0;
	assign cs = ( state == idle ) ? 1 : 0;
	assign output_axis_tvalid = output_axis_tvalid_reg;
	assign output_axis_tdata = output_axis_tdata_reg;

	// begin state machine
	always @(state)
	begin
		case (state)
			idle:
			begin
				output_axis_tdata_reg <= output_axis_tdata_reg;
				output_axis_tvalid_reg <= 0;
			end
			load:
			begin
				output_axis_tdata_reg <= output_axis_tdata_reg;
				output_axis_tvalid_reg <= 1;
			end
			transact:
			begin
				output_axis_tdata_reg <= output_axis_tdata_reg;
				output_axis_tvalid_reg <= 0;
			end
			unload:
			begin
				output_axis_tdata_reg <= shift;
				output_axis_tvalid_reg <= 1;
			end
			default:
				state = idle;
		endcase
	end
	
	always @(posedge clk)
	begin
		if (rst)
		begin
			state <= idle;
			//shift <= 0;
			//count <= 0;
			//output_axis_tdata_reg <= 0;
		end
		else
			case (state)
				idle:
					if ( input_axis_tvalid )
						state = load;
				load:
					state = transact;
				transact:
					if ( |count )
						state = transact;
					else
						state = unload;
				unload:
					if ( input_axis_tvalid )
						state = load;
					else
						state = idle;
			endcase
			// end state machine
		if ( state == transact )
			miso_d <=  miso;
	end

	always @(negedge clk)
	begin
		// Shift Data
		case (state)
			idle:
			begin
				shift <= shift;
				count <= count;
			end
			load:
			begin
				shift <= input_axis_tdata;
				count <= DATA_WIDTH;
			end
			transact:
			begin
				shift <= {shift[DATA_WIDTH-2:0], miso_d};
				count <= count - 1;
			end
			unload:
			begin
				shift <= 0;
				count <= 0;
			end
		endcase
	end

endmodule

