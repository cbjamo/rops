module top
(
	input uart_txd_in,
	output uart_rxd_out,

	//input SDO, INT, DRDY_M,
	//output SDI, SCK, CS_AG, CS_M, CS_ALT,

	input CLK100MHZ,
	input ck_rst,
	input [3:0] sw,
	input [3:0] btn,
	input [3:0] led,
	inout [3:0] jb,
	inout [3:0] jc,
	inout [3:0] jd
);
	wire [3:0] dat_a [1:0];
	assign dat_a[0] = jc;
	assign dat_a[1] = jd;

	bidir_mux
	#(
		.BITS( 4 ),
		.INOUTS( 2 )
	)
	bidir_mux_inst
	(
		.select_i( sw[0] ),
		.oe_i( 1'b1 ),
		.dir_i( sw[1] ),
		.dat_a( dat_a ),
		.dat_b( jb )
	);

endmodule
