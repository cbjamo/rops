module wb_intercon
#(
	parameter MASTERS = 2,
	parameter SLAVES = 4,

	parameter DAT_WIDTH = 32,
	parameter ADR_WIDTH = 32,
	parameter SEL_WIDTH = (ADR_WIDTH / 8 ),

	parameter [ADR_WIDTH-1:0] SLAVE_ADR_MASK [SLAVES-1:0] = { 32'd0 }
)
(
	// Master connections
	input [ADR_WIDTH-1:0] adr_i [MASTERS-1:0],
	input [SEL_WIDTH-1:0] sel_i [MASTERS-1:0],
	input [DAT_WIDTH-1:0] mosi_dat_i [MASTERS-1:0],
	output [DAT_WIDTH-1:0] miso_dat_o,
	input stb_i [MASTERS-1:0],
	input we_i [MASTERS-1:0],
	output ack_o [MASTERS-1:0],
	output err_o [MASTERS-1:0],
	output rty_o [MASTERS-1:0],

	// Slave connections
	output [ADR_WIDTH-1:0] adr_o,
	output [SEL_WIDTH-1:0] sel_o,
	input [DAT_WIDTH-1:0] miso_dat_i [SLAVES-1:0],
	output [DAT_WIDTH-1:0] mosi_dat_o,
	output stb_o [SLAVES-1:0],
	output we_o,
	input ack_i [SLAVES-1:0],
	input err_i [SLAVES-1:0],
	input rty_i [SLAVES-1:0],

	// Arbiter connections
	input gnt_i [MASTERS-1:0],
	input cyc_i

);
	wire acmp [SLAVES-1:0];
	wire ack, err, rty;
	wire stb;
	wire [DAT_WIDTH-1:0] dat;
	wire [ADR_WIDTH-1:0] adr;

	assign acmp_d = { << {acmp}};
	assign gnt_d = { << {gnt_i}};

	wire [SLAVES-1:0] ack_d;
	wire [SLAVES-1:0] err_d;
	wire [SLAVES-1:0] rty_d;

	assign ack_d = { << {ack_i}};
	assign err_d = { << {err_i}};
	assign rty_d = { << {rty_i}};

	assign ack = |ack_d;
	assign err = |err_d;
	assign rty = |rty_d;

	generate
		for (genvar i = MASTERS-1; i > 0 ; i--)
		begin: per_master_generate
			// Master ack/err/rty logic
			assign ack_o[i] = ack && gnt_i[i];
			assign err_o[i] = err && gnt_i[i];
			assign rty_o[i] = rty && gnt_i[i];
		end
	endgenerate

	generate
		for (genvar j = SLAVES-1; j > 0; j--)
		begin: per_slave_generate
			// Slave coarse address decoding 
			// TODO maybe need more than this
			assign acmp[j] = adr_o && SLAVE_ADR_MASK[j];

			// Slave strobe inputs
			assign stb_o[j] = acmp[j] && cyc_i && stb;
		end
	endgenerate

	// MISO dat mux
	onehot_mux #( .BITS(DAT_WIDTH), .INPUTS(SLAVES) ) miso_mux ( .select_i(acmp_d), .dat_i(miso_dat_i), .dat_o(miso_dat_o) );

	// MOSI dat onehot_mux
	onehot_mux #( .BITS(DAT_WIDTH), .INPUTS(MASTERS) ) mosi_mux ( .select_i(gnt_d), .dat_i(mosi_dat_i), .dat_o(mosi_dat_o) );

	// Address onehot_mux
	onehot_mux #( .BITS(ADR_WIDTH), .INPUTS(MASTERS) ) adr_mux ( .select_i(gnt_d), .dat_i(adr_i), .dat_o(adr_o) );

	// Select onehot_mux
	onehot_mux #( .BITS(SEL_WIDTH), .INPUTS(MASTERS) ) sel_mux ( .select_i(gnt_d), .dat_i(sel_i), .dat_o(sel_o) );

	// Write Enable onehot_mux
	onehot_mux #( .BITS( 1'b1 ), .INPUTS(MASTERS) ) we_mux ( .select_i(gnt_d), .dat_i(we_i), .dat_o(we_o) );

	// Strobe Output onehot_mux
	onehot_mux #( .BITS( 1'b1 ), .INPUTS(MASTERS) ) stb_mux ( .select_i(gnt_d), .dat_i(stb_i), .dat_o(stb) );

endmodule
