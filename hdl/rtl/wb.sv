module top
(
	input uart_txd_in,
	output uart_rxd_out,

	//input SDO, INT, DRDY_M,
	//output SDI, SCK, CS_AG, CS_M, CS_ALT,

	input CLK100MHZ,
	input ck_rst,
	output wire [3:0] led
);
	localparam MASTERS = 1;
	localparam SLAVES = 1;

	assign uart_rxd_out = uart_txd_in;
	assign led = wb_mi_dat;

	localparam WB_DAT_WIDTH = 32;
	localparam WB_ADR_WIDTH = 32;
	localparam WB_SEL_WIDTH = WB_ADR_WIDTH / 8;

	// Syscon Connections
	wire wb_clk;
	wire wb_rst;

	// Master Connections
	wire [WB_ADR_WIDTH-1:0] wb_mo_adr_a [MASTERS-1:0];
	wire [WB_SEL_WIDTH-1:0] wb_mo_sel_a [MASTERS-1:0];
	wire [WB_DAT_WIDTH-1:0] wb_mo_dat_a [MASTERS-1:0];
	wire [WB_DAT_WIDTH-1:0]	wb_mi_dat;
	wire wb_mo_we_a [MASTERS-1:0];
	wire wb_mo_stb_a [MASTERS-1:0];
	wire wb_mi_ack_a [MASTERS-1:0];
	wire wb_mi_err_a [MASTERS-1:0];
	wire wb_mi_rty_a [MASTERS-1:0];
	wire wb_mo_cyc_a [MASTERS-1:0];

	// Slave Connections
	wire [WB_ADR_WIDTH-1:0] wb_si_adr;
	wire [WB_SEL_WIDTH-1:0] wb_si_sel;
	wire [WB_DAT_WIDTH-1:0] wb_so_dat_a [SLAVES-1:0];
	wire [WB_DAT_WIDTH-1:0]	wb_si_dat;
	wire wb_si_we_a [SLAVES-1:0];
	wire wb_si_stb_a [SLAVES-1:0];
	wire wb_so_ack_a [SLAVES-1:0];
	wire wb_so_err_a [SLAVES-1:0];
	wire wb_so_rty_a [SLAVES-1:0];

	// Arbiter Connections
	wire wb_gnt_a [MASTERS-1:0];
	wire wb_cyc;

	wb_syscon
	#(
		.REF_CLK_HZ( 100000000 ),
		.WB_CLK_HZ( 400000000 )
	)
	wb_syscon_inst
	(
		.ref_clk_i( CLK100MHZ ),
		.m_rst_n_i( ck_rst ),

		.wb_clk_o( wb_clk ),
		.wb_rst_o( wb_rst )
	);

	wb_intercon
	#(
		.MASTERS( MASTERS ),
		.SLAVES( SLAVES ),

		.DAT_WIDTH( WB_DAT_WIDTH ),
		.ADR_WIDTH( WB_ADR_WIDTH ),

		.SLAVE_ADR_MASK( {
			32'd0
		} )
	)
	wb_intercon_inst
	(
		// Master Connections
		.adr_i( wb_mo_adr_a ),
		.sel_i( wb_mo_sel_a ),
		.mosi_dat_i( wb_mo_dat_a ),
		.miso_dat_o( wb_so_dat ),
		.we_i( wb_mo_we_a ),
		.stb_i( wb_mo_stb_a ),
		.ack_o( wb_mi_ack_a ),
		.err_o( wb_mi_err_a ),
		.rty_o( wb_mi_rty_a ),

		// Slave Connections
		.adr_o( wb_si_adr ),
		.sel_o( wb_si_sel ),
		.mosi_dat_o( wb_si_dat ),
		.miso_dat_i( wb_so_dat_a ),
		.we_o( wb_si_we ),
		.stb_o( wb_si_stb_a ),
		.ack_i( wb_so_ack_a ),
		.err_i( wb_so_err_a ),
		.rty_i( wb_so_rty_a ),

		// Arbiter Connections
		.gnt_i( wb_gnt_a ),
		.cyc_i( wb_cyc )

	);

	hbexec
	#(
	)
	hb_exec_inst
	(
		.i_clk( wb_clk ),
		.i_reset( wb_rst ),
        // The input command channel
        .i_cmd_stb(),
		.i_cmd_word(),
		.o_cmd_busy(),
        // The return command channel
        .o_rsp_stb(),
		.o_rsp_word(),
        // Our wishbone outputs
        .o_wb_cyc( wb_mo_cyc_a[0] ),
		.o_wb_stb( wb_mo_stb_a[0] ),
        .o_wb_we( wb_mo_we_a[0] ),
		.o_wb_addr( wb_mo_adr_a[0] ),
		.o_wb_data( wb_mo_dat_a[0] ),
		.o_wb_sel( wb_mo_sel_a[0] ),
        // The return wishbone path
        .i_wb_ack( wb_mi_ack_a[0] ),
		.i_wb_stall( 1'b0 ),
		.i_wb_err( wb_mi_err_a[0] ),
		.i_wb_data( wb_mi_dat )
	);


	wb_ram
	#(
		.DATA_WIDTH( WB_DAT_WIDTH ),
		.ADDR_WIDTH( WB_ADR_WIDTH ),
		.SELECT_WIDTH( WB_SEL_WIDTH )
	)
	(
		.clk( wb_clk ),
		.adr_i( wb_si_adr ),
		.sel_i( wb_si_sel ),
		.dat_i( wb_si_dat ),
		.dat_o( wb_so_dat_a[0] ),
		.we_i( wb_si_we ),
		.stb_i( wb_si_stb_a[0] ),
		.ack_o( wb_so_ack_a[0] ),
		.cyc_i( wb_cyc )
	);

endmodule
