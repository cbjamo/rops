// Language: Verilog 2012

`timescale 1ns / 1ps

/*
 * Wishbone IOMUX
 */
module wb_iomux #
(
	parameter PIN_WIDTH = 8,
	parameter PORT_WIDTH = 16,

	parameter DATA_WIDTH = 32,				// width of data bus in bits (8, 16, 32, or 64)
	parameter ADDR_WIDTH = 32,				// width of address bus in bits
	parameter SELECT_WIDTH = (DATA_WIDTH/8)	// width of word select bus (1, 2, 4, or 8)
)
(
	// Master clk and rst
	input  wire clk,
	input  wire rst,
/*
	// Wishbone
	input  wire [ADDR_WIDTH-1:0] adr_i,		// ADR_I() address
	input  wire [DATA_WIDTH-1:0] dat_i,		// DAT_I() data in
	output wire [DATA_WIDTH-1:0] dat_o,		// DAT_O() data out
	input  wire we_i,						// WE_I write enable input
	input  wire [SELECT_WIDTH-1:0] sel_i,	// SEL_I() select input
	input  wire stb_i,						// STB_I strobe input
	output wire ack_o,						// ACK_O acknowledge output
	input  wire cyc_i,						// CYC_I cycle input
*/
	input reg [$clog2(PIN_WIDTH):0] port_reg_a [PORT_WIDTH-1:0],
	input reg [$clog2(PORT_WIDTH):0] pin_reg_a [PIN_WIDTH-1:0],
	// IO
	inout pins [PIN_WIDTH-1:0],
	inout ports [PORT_WIDTH-1:0]
);
/*
	reg [$clog2(PIN_WIDTH):0] port_reg_a [PORT_WIDTH-1:0];
	reg [$clog2(PORT_WIDTH):0] pin_reg_a [PIN_WIDTH-1:0];
*/

	// Port Muxes
	integer i;
	for (i = 0; i < PORT_WIDTH; i++) begin : port_muxes
		mux 
		#( .BITS( 1'b1 ), .INPUTS( PORT_WIDTH ) ) 
		port_mux_inst 
		( .select_i( port_reg_a[i][$clog2(PIN_WIDTH)-1:0] ), .dat_i( pins ), .dat_o( port[i] );

	// Pin Muxes
	integer j;
	for (j = 0; j < PIN_WIDTH; j++) begin : pin_muxes
		mux
		#( .BITS( 1'b1 ), INPUTS( PIN_WIDTH ) )
		pin_mux_inst
		( .select_i( pin_reg_a[j][[$clog2(PORT_WIDTH)-1:0] ), .dat_i( ports ), .dat_o( pin[j] );

endmodule
