module debouncer(
    input clk,
    input d,  // "d" is the glitchy, asynchronous to clk, active low push-button signal

    // from which we make three outputs, all synchronous to the clock
    output reg d_state,  // 1 as long as the push-button is active (down)
    output d_down,  // 1 for one clock cycle when the push-button goes down (i.e. just pushed)
    output d_up   // 1 for one clock cycle when the push-button goes up (i.e. just released)
);

// First use two flip-flops to synchronize the d signal the "clk" clock domain
reg d_sync_0;  always @(posedge clk) d_sync_0 <= ~d;  // invert d to make d_sync_0 active high
reg d_sync_1;  always @(posedge clk) d_sync_1 <= d_sync_0;

// Next declare a 16-bits counter
reg [17:0] d_cnt;

// When the push-button is pushed or released, we increment the counter
// The counter has to be maxed out before we decide that the push-button state has changed

wire d_idle = (d_state==d_sync_1);
wire d_cnt_max = &d_cnt;	// true when all bits of d_cnt are 1's

always @(posedge clk)
if(d_idle)
    d_cnt <= 0;  // nothing's going on
else
begin
    d_cnt <= d_cnt + 16'd1;  // something's going on, increment the counter
    if(d_cnt_max) d_state <= ~d_state;  // if the counter is maxed out, d changed!
end

assign d_down = ~d_idle & d_cnt_max & ~d_state;
assign d_up   = ~d_idle & d_cnt_max &  d_state;
endmodule
