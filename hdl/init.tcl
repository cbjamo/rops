# Make the vivado project
create_project ROPS xbuild -part xc7a35ticsg324-1L
add_files -norecurse -force {rtl/}
add_files -fileset constrs_1 -norecurse -force rtl/constraints.xdc
update_compile_order -fileset sources_1

exit

