# Hardware

## Sensors

* IMU
	* [ST iNEMO LSM9DS1](http://www.st.com/content/st_com/en/products/mems-and-sensors/inemo-inertial-modules/lsm9ds1.html)
		* 3 axis accel - 2,4,6,16 g, 16 bit, 14.9 to 952Hz
		* 3 axis gyro - 245,500,2000 dps, 16 bit, 14.9 to 952Hz
		* 3 axis mag - 4,8,12,16 gauss, 16 bit. 0.625 to 80hz
		* Embedded temp
		* 1.9 to 3.6V supply
		* I2C and SPI
		* 4.73$ @ 100
* Barometer
	* [ST LP35HW/LPS33HW (barbed)](http://www.st.com/en/mems-and-sensors/lps35hw.html)
		* 260 to 1260 hPa (10000 to -1800m @ 15C) absolute, 24 bit
		* 16 bit temperature
		* 1 to 75Hz
		* 1.7 to 3.6V supply
		* I2C and SPI
		* 4.95$ @ 100 (35), 6.60$ @ 100 (33)
* GPS
	* [NEO-M8T](https://www.u-blox.com/en/product/neolea-m8t-series)
		* BeiDou, Galileo, GLONASS, GPS / QZSS
		* Raw outputs (enables RTK)
		* 10Hz
		* 2.7 to 3.6V supply
		* UART, USB, SPI, I2C
		* 34.42$ @ 100, 80.99$ @ 1
* RGB/Stereo
* RGBD
	* [Intel Realsense D435](https://click.intel.com/intelr-realsensetm-depth-camera-d435.html)
		* Depth: FOV 65.5x100.6, 720p, 90fps
		* RGB: 1080p, 30fps
		* 0.2 - 10m
		* USB3
		* 179$ @ 1
* Lidar
	* [RPLIDAR A2](https://www.slamtec.com/en/Lidar/A2Spec)
		* 360 degree, 0.9 degree res
		* 0.15 - 6m, .5mm res
		* 10Hz scan rate 
		* 5v supply
		* 3.3v UART, PWM motor control
		* 449$ @ 1

## I/O

* Serial
	* TTL
	* RS232
	* RS485
	* SPI
	* I2C
	* Ethernet
	* CAN
* GPIO
* Analog

* Connectors
	* Card-edge - 48 io
	* PMOD
